function MaxUnrepeatedSub(s) {
    let maxStr = '';
    let repeatIndex = -1;
    for (let i = 0; i < s.length; i++) {
        repeatIndex = maxStr.indexOf(s[i]);
        if (repeatIndex >= 0) {
            break;
        }
        maxStr += s[i];
    }

    // console.log(s, repeatIndex)

    if (repeatIndex == -1) {
        return maxStr;
    }
    let rightMaxStr = MaxUnrepeatedSub(s.substr(repeatIndex + 1));
    if (maxStr.length < rightMaxStr.length) {
        return rightMaxStr;
    }
    return maxStr;
}

let testcases = {
    '': { result: '', expect: true },
    bbbbb: { result: 'b', expect: true },
    abcabcbb: { result: 'abc', expect: true },
    abcdefg: { result: 'abcdefg', expect: true },
    abccccbbadbad: { result: 'abc', expect: true },
    abcabcdbdd: { result: 'abcd', expect: true },
    abcxdcccbbadbad: { result: 'abc', expect: false },
    pwwkew: { result: 'wke', expect: true },
    abcbcd: { result: 'bcd', expect: false },
};

for (let k in testcases) {
    let r = MaxUnrepeatedSub(k);
    if ((testcases[k].result == r) != testcases[k].expect) {
        console.error('Not Match: input ' + k + ' output ' + MaxUnrepeatedSub(k) + ' expect ' + testcases[k]);
    }
}
