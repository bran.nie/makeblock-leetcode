var lengthOfLongestSubstring = function (s) {
    // 创建贪吃蛇～
    let snake = '';
    let max = 0;

    // 遍历字符串
    for (let i = 0; i < s.length; i++) {
        // 当前字符在 snake 中的位置
        const s_i = snake.indexOf(s[i]);
        if (s_i === -1) {
            // 贪吃蛇中没有该字符，可以吃掉
            snake += s[i];
        } else {
            // 贪吃蛇有重复的字符，则从重复的地方截断、再吃掉当前字符
            snake = snake.substring(s_i + 1) + s[i];
        }
        // console.log({ snake });
        // 记录贪吃蛇最大的长度
        max = Math.max(max, snake.length);
    }

    return max;
};

/**
 * 贪吃蛇的方式，在检测是否重复的字符时，使用的 indexof，indexof 也是一个遍历，在极端情况下，这个相当于两层遍历，所以，对上面方法的优化，就在于如何更快地检测。
 * 说到检测，那么哈希表就是一个 O(1) 的方式。
 * ES6 的 Set 数据结构，是一个哈希集合，相比对象，它的一些方法如 add, has, size 等等更加语义化，也就是更适合用在这里，
 */

var lengthOfLongestSubstring2 = function (s) {
    // 滑动窗口的方式，贪吃蛇的优化。
    const scrollWindow = new Set();
    let max = 0;
    // 声明窗口在字符串中的起始下标
    let windowStart = 0;

    for (let i = 0; i < s.length; i++) {
        // 当前遍历的元素
        const item = s[i];
        // 滑动窗口中没有这个元素时，滑动窗口增加该元素，即窗口变大
        if (!scrollWindow.has(item)) {
            scrollWindow.add(item);
        } else {
            // 滑动窗口已有该元素，则将窗口左边开始到这个元素，都 remove 掉。
            while (windowStart < s.length) {
                const cur = s[windowStart];
                scrollWindow.delete(cur);
                windowStart++;

                if (cur === item) {
                    scrollWindow.add(cur);
                    break;
                }
            }
        }

        // console.log({ scrollWindow });
        max = Math.max(max, scrollWindow.size);
    }

    return max;
};

const testcases = [
    {
        str: '',
        expect: 0,
    },
    {
        str: ' ',
        expect: 1,
    },
    {
        str: 'abcdefg',
        expect: 7,
    },
    {
        str: 'aaaaaa',
        expect: 1,
    },
    {
        str: 'abcabcbb',
        expect: 3,
    },
    {
        str: 'abcabcdb',
        expect: 4,
    },
    {
        str: 'abcbcabb',
        expect: 3,
    },
];

function testFn(fn, testcases) {
    console.group(`测试函数：${fn.name}，测试集 total：${testcases.length}`);
    for (let testcase of testcases) {
        const r1 = fn(testcase.str);

        if (testcase.expect !== r1) {
            console.warn(`${testcase.str} 期望的结果是：${testcase.expect}，函数 ${fn.name} 的返回值是: ${r1}`);
        } else {
            console.count('pass');
        }
    }
    console.groupEnd();
    console.countReset('pass');
}

testFn(lengthOfLongestSubstring, testcases);
testFn(lengthOfLongestSubstring2, testcases);
