//https://leetcode-cn.com/problems/container-with-most-water/
function calcMaxWater(arrA) {
    let memo = new Array(arrA.length).fill(null).map(() => {
        return {
            water: 0,
            peerIndex: -1,
        };
    });

    //j 必须 >= i
    let calcWater = (i, j) => Math.min(arrA[i], arrA[j]) * (j - i);

    for (let i = 0; i < arrA.length; i++) {
        let cv = arrA[i];
        let leftFarIndex = i;
        for (let j = i; j >= 0; j--) {
            if (arrA[j] < cv) {
                continue;
            }
            leftFarIndex = j;
        }
        //   console.log("far ", leftFarIndex, i)
        if (leftFarIndex != i) {
            memo[i].water = calcWater(leftFarIndex, i);
            memo[i].peerIndex = leftFarIndex;
        }

        //注意leftFarIndex左边的值必然比arrA[i]小, 因此需要遍历处理
        for (let j = leftFarIndex - 1; j >= 0; j--) {
            let v = calcWater(j, i);
            if (v > memo[j].water) {
                memo[j].water = v;
                memo[j].peerIndex = i;
            }
        }
    }

    let maxWater = 0;
    for (let i = 0; i < memo.length; i++) {
        if (maxWater < memo[i].water) {
            maxWater = memo[i].water;
        }
    }
    return maxWater;
}

let testcases = [
    { input: [1, 8, 6, 2, 5, 4, 8, 3, 7], expect: 49 },
    { input: [1, 1], expect: 1 },
    { input: [1, 2], expect: 1 },
    { input: [2, 3, 4, 5, 18, 17, 6], expect: 17 },
    { input: [4, 3, 2, 1, 4], expect: 16 },
    { input: [1, 2, 1], expect: 2 },
];

for (let k in testcases) {
    let r = calcMaxWater(testcases[k].input);
    if (testcases[k].expect != r) {
        console.error('Not Match: input [' + testcases[k].input + '], output ' + r + ', Expect ' + testcases[k].expect);
    }
}
